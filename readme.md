# RuneMate Gradle Plugin

[![Gradle Plugin Portal](https://img.shields.io/gradle-plugin-portal/v/com.runemate)](https://plugins.gradle.org/plugin/com.runemate)  
This plugin provides a set of integration utilities to assist development on the RuneMate platform.

## Usage

Simply add the following to your Gradle buildscript to get started:

```kotlin
plugins {
    id("com.runemate") version "1.5.1"
}

runemate {
    devMode = true
    autoLogin = true
}
```

### Configuration

The full list of configuration options is below:

| Option                    | Description                                                                              | Default                                                                                                                                                                                         |
|---------------------------|------------------------------------------------------------------------------------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| autoLogin                 | Tells the client to attempt automatic login                                              | false                                                                                                                                                                                           |
| devMode                   | Tells the client to launch in developer mode                                             | true                                                                                                                                                                                            |
| debug                     | Tells the client to enable debug logging                                                 | false                                                                                                                                                                                           |
| apiVersion                | Tells Gradle which version of runemate-game-api to fetch from Maven                      | + (latest)                                                                                                                                                                                      |
| clientVersion             | Tells Gradle which version of runemate-client to fetch from Maven                        | + (latest)                                                                                                                                                                                      |
| botDirectories            | Tells the client which directories to scan for bots                                      | `$projectDir/build/libs` (project build directory)                                                                                                                                              |
| allowExternalDependencies | Tells Gradle to allow dependency resolution for external dependencies                    | false                                                                                                                                                                                           |
| excludeFromSubmission     | Tells Gradle to exclude the module this extension is registered to from store submission | false                                                                                                                                                                                           |
| submissionToken           | The token used to authenticate when publishing your products (see Publishing to Store)   | The first non-null value in order: this property value, project property `runemateSubmissionToken`, system property `runemateSubmissionToken`, environment variable `RUNEMATE_SUBMISSION_TOKEN` |

### Launching the client

The plugin adds the `runClient` task to your Gradle project, this will launch the client using the configuration as described above.

Type the following command:

```
./gradlew runClient
```

### Manifest Generation

Manifests can be declared in the `manifests` block of the configuration, for example:

```kotlin
runemate {
    //... other configuration

    manifests {
        create("My Product Name") {
            //... manifest elements
        }
    }
}
```

These manifest declarations will be used to generate JSON manifests in `$projectDir/build/runemate/sources/.runemate`.

A full example manifest block:

```kotlin
manifests {
    create("Development Toolkit") {
        //Required: mainClass, tagline, description, version, internalId
        mainClass = "com.runemate.bots.dev.DevelopmentToolkit"
        tagline = "RuneMate's Swiss Army Knife"
        description = "Presents runtime data, such as the loaded NPCs, to assist in bot development."
        version = "2.5.1"
        internalId = "devkit"

        //Optional: Everything else
        access = Access.PUBLIC
        hidden = false
        categories(Category.DEVELOPER_TOOLS)
        tags("devkit", "debug")
        resources {
            include("css/DevelopmentToolkitPage.css")
            include("fxml/DevelopmentToolkitPage.fxml")
            include("fxml/QueryBuilderPage.fxml")
        }

        //Tailors the pricing and trial window of the product
        price(0.20)
        trial {
            window = Duration.ofDays(7) //or window("ISO-8601 duration format")
            allowance = Duration.ofHours(3) //or allowance("ISO-8601 duration format")
        }

        //Advanced: Used to declare the use and optionality of advanced features
        features {
            required(FeatureType.DIRECT_INPUT)
        }

        //Advanced: Used to tailor obfuscation
        obfuscation {
            exclude("class.i.dont.want.to.Obfuscate")
        }

        //Enabling this will cause the plugin to skip this manifest during the generation phase
        skip = false
    }
}
```

### Publishing to store

The plugin adds the `submitForReview` task to the root project. This task scans each project with the plugin applied and performs the
following tasks in order:

1. Deletes any previous submission bundles or generated manifests
2. Generates manifests as declared by the `manifests` block of each project
3. Scans and validates any manually produced manifests
4. Bundles all source code, resources and manifests into the root project build directory
5. Submits the source bundle to the store for review and publication

This requires the `submissionToken` to be set (see the Configuration section). The best way to do this is to add a property
called `runemateSubmissionToken`
in `$USER_HOME/.gradle/gradle.properties`:

```
runemateSubmissionToken=YOUR_TOKEN
```

The submission token itself can be acquired from the [RuneMate developer panel](https://www20230922135246.runemate.com/developer).

If you want to preview the contents that will be submitted, run the `buildSubmission`, this will generate a tarball
in `$rootProject/build/runemate/distribution`.

### Multi-module Gradle builds

The plugin supports multi-module Gradle builds with a few caveats:

- Each module with source code or resources needs to have the Gradle plugin applied. You can apply the plugin to all subprojects using
  the `subprojects` block:

```kotlin
subprojects {
    apply<JavaPlugin>()
    apply<RuneMatePlugin>()
}
```

- The `runClient` task is added to all projects with the plugin applied. If you run this task from the Gradle tool window in IDEA
  (or by failing to specify the project in the command line) then a client will be launched for every project. To change this behaviour, you
  can disable the task in each of your subprojects:

```kotlin
subprojects {
    tasks.runClient {
        enabled = false
    }
}
```

- The client may fail to find manifests, bot classes or resources. You can assist it by building a JAR that contains all of your source
  code, manifests and resources by adding the following task to your root Gradle buildscript:

```kotlin
val uberJar by tasks.registering(Jar::class) {
    group = "runemate"
    dependsOn(subprojects.map { it.tasks.assemble })
    archiveBaseName.set("runemate-bots")
    subprojects.forEach {
        from(it.sourceSets.main.get().output)
        from(it.layout.buildDirectory.dir("runemate/sources/.runemate"))
    }
}

tasks.runClient {
    dependsOn(uberJar)
}
```

## Building the plugin

To build the plugin, just type the following command:

```
./gradlew build
```