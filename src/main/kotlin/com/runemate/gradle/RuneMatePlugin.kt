@file:Suppress("unused")

package com.runemate.gradle

import org.gradle.api.GradleException
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.plugins.JavaPlugin
import org.gradle.api.plugins.JavaPluginExtension
import org.gradle.api.tasks.JavaExec
import org.gradle.jvm.toolchain.JavaLanguageVersion
import org.gradle.kotlin.dsl.*
import org.openjfx.gradle.JavaFXOptions
import org.openjfx.gradle.JavaFXPlugin
import java.io.File
import java.util.concurrent.TimeUnit

class RuneMatePlugin : Plugin<Project> {

    override fun apply(target: Project): Unit = with(target) {
        configurePlugins()
        configureDependencies()
        createPublishTasks()
        createDeveloperTasks()
    }

    private fun Project.configurePlugins() {
        apply<JavaPlugin>()
        apply<JavaFXPlugin>()

        configurations.create(RUNEMATE) {
            configurations["implementation"].extendsFrom(this)
        }

        extensions.create<RuneMateExtension>(RUNEMATE).apply {
            autoLogin.convention(false)
            devMode.convention(true)
            debug.convention(false)
            noReloading.convention(false)
            apiVersion.convention("+")
            clientVersion.convention("+")
            pathfinderVersion.convention("+")
            botDirectories.convention(setOf(layout.buildDirectory.dir("libs")))
            excludeFromSubmission.convention(false)
            allowExternalDependencies.convention(false)
        }

        configure<JavaPluginExtension> {
            toolchain {
                languageVersion.set(JavaLanguageVersion.of(17))
            }
        }

        configure<JavaFXOptions> {
            version = JAVAFX_VERSION
            modules(
                    "javafx.base",
                    "javafx.fxml",
                    "javafx.controls",
                    "javafx.media",
                    "javafx.web",
                    "javafx.graphics",
                    "javafx.swing"
            )
        }

    }

    private fun Project.configureDependencies() {
        configurations.all {
            resolutionStrategy {
                cacheChangingModulesFor(0, TimeUnit.SECONDS)
                eachDependency {
                    if (requested.group == "com.runemate") {
                        val rm = project.extensions.getByType<RuneMateExtension>()
                        when (requested.name) {
                            "runemate-client"   -> useVersion(rm.clientVersion.get())
                            "runemate-game-api" -> useVersion(rm.apiVersion.get())
                            "runemate-pathfinder-api" -> useVersion(rm.pathfinderVersion.get())
                        }
                    }
                }
            }
        }

        configurations.getByName("runtimeClasspath") {
            resolutionStrategy.eachDependency {
                val external = requested.name !in rootProject.allprojects.map { it.name }
                val key = "${requested.group}:${requested.name}"
                if (external && dependencyAllowList.none { it matches key }) {
                    val allow = extensions.getByType<RuneMateExtension>().allowExternalDependencies.get()
                    if (allow) {
                        logger.warn("RuneMate does not support external dependencies, please remove: $key ($requested)")
                    } else {
                        throw GradleException("RuneMate does not support external dependencies, please remove: $key")
                    }
                }
            }
        }

        repositories {
            runemateGameApiRepository()
            runemateClientRepository()
            runematePathfinderRepository()
            mavenCentral()

            //Snapshot testing
            mavenLocal {
                content {
                    includeVersionByRegex("com.runemate", "runemate-pathfinder-api", ".*-SNAPSHOT")
                    includeVersionByRegex("com.runemate", "runemate-game-api", ".*-SNAPSHOT")
                    includeVersionByRegex("com.runemate", "runemate-client", ".*-SNAPSHOT")
                    includeVersionByRegex("com.runemate", "runemate-pathfinder-api", ".*-beta.*")
                    includeVersionByRegex("com.runemate", "runemate-game-api", ".*-beta.*")
                    includeVersionByRegex("com.runemate", "runemate-client", ".*-beta.*")
                }
            }
        }

        dependencies {
            configurations[RUNEMATE]("com.runemate:runemate-client") {
                isChanging = true
            }
            configurations[RUNEMATE]("com.runemate:runemate-game-api") {
                isChanging = true
            }
            configurations[RUNEMATE]("com.runemate:runemate-pathfinder-api") {
                exclude(group = "com.runemate", module = "runemate-pathfinder-data")
                isChanging = true
            }
            configurations["compileOnly"]("org.projectlombok:lombok:1.18.32")
            configurations["annotationProcessor"]("org.projectlombok:lombok:1.18.32")
        }
    }

    private fun Project.createDeveloperTasks() {
        tasks.register("runClient", JavaExec::class) {
            group = RUNEMATE
            classpath = configurations["runtimeClasspath"]
            mainClass.set("com.runemate.client.boot.Boot")

            arrayOf(
                    "java.base/java.lang.reflect",
                    "java.base/java.nio",
                    "java.base/sun.nio.ch",
                    "java.base/java.util.regex",
                    "java.base/java.util.concurrent"
            ).forEach {
                jvmArgs("--add-opens=${it}=ALL-UNNAMED")
            }

            //Handle arguments
            val ext = project.extensions.getByType<RuneMateExtension>()
            ext.autoLogin.ifTrue { args("--login") }
            ext.devMode.ifTrue { args("--dev") }
            ext.debug.ifTrue { args("--debug") }
            ext.noReloading.ifTrue { args("--no-reloading") }
            ext.botDirectories.ifPresent {
                val dirs = it.map(::file)
                    .filter(File::exists)
                    .joinToString(File.pathSeparator)

                if (dirs.isNotEmpty()) {
                    args("-d", dirs)
                }
            }

            doFirst {
                logger.lifecycle("Launching RuneMate client with args {}", args)
            }
        }

        //Check if there are multiple instances of 'runClient' in the task graph
        gradle.taskGraph.whenReady {
            if (allTasks.count { it.name == "runClient" && it.enabled } > 1) {
                logger.warn(
                        "w: We've detected that you have the RuneMate plugin applied to multiple projects. We suggest that you disable the 'runClient' task for subprojects:\n"
                                + """
                    
                    ${rootProject.buildscript.sourceFile}:
                    subprojects {
                        tasks.runClient {
                            enabled = false
                        }
                    }
                """.trimIndent()
                )
            }
        }
    }
}