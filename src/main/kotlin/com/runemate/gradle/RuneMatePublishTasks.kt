package com.runemate.gradle

import com.fasterxml.jackson.module.kotlin.readValue
import com.runemate.metadata.bot.isManifest
import jsonMapper
import okhttp3.MediaType.Companion.toMediaType
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.RequestBody.Companion.asRequestBody
import org.gradle.api.DefaultTask
import org.gradle.api.GradleException
import org.gradle.api.Project
import org.gradle.api.tasks.Copy
import org.gradle.api.tasks.Delete
import org.gradle.api.tasks.TaskAction
import org.gradle.api.tasks.bundling.Compression
import org.gradle.api.tasks.bundling.Jar
import org.gradle.api.tasks.bundling.Tar
import org.gradle.kotlin.dsl.findByType
import org.gradle.kotlin.dsl.get
import org.gradle.kotlin.dsl.getByType
import org.gradle.kotlin.dsl.register
import parse
import toJson
import validate
import java.io.IOException
import java.nio.file.Files
import java.util.*
import kotlin.io.path.relativeTo
import kotlin.io.path.writeText

private val Project.runeMateBuildDir get() = layout.buildDirectory.dir("runemate")
private val Project.runeMateSourcesDir get() = runeMateBuildDir.map { it.dir("sources") }
private val Project.runeMateDistributionDir get() = runeMateBuildDir.map { it.dir("distribution") }
private val Project.runeMateManifestDir get() = runeMateBuildDir.map { it.dir("sources") }.map { it.dir(".runemate") }
private val Project.excludeFromSubmission : Boolean
    get() {
        val ext = extensions.findByType<RuneMateExtension>() ?: return true
        return ext.excludeFromSubmission.get() || (ext.manifests.isNotEmpty() && ext.manifests.all { mf -> mf.skip })
    }

private const val TASK_GENERATE_MANIFESTS = "generateManifests"
private const val TASK_VALIDATE_MANIFESTS = "validateManifests"
private const val TASK_COLLECT_SOURCES = "collectSubmissionSources"
private const val TASK_BUILD_SUBMISSION = "buildSubmission"
private const val TASK_CLEAN_SUBMISSION = "cleanSubmission"

private const val SUBMISSION_URL = "https://www.runemate.com/developer/submit"

/*
  1. generate - create manifests from the declarations
  2. validate - check all manifests are valid
  3. gather sources/resources - copy sources into build directory
  4. create bundle - create distribution .tar.gz
  5. publish - submit bundle for review
*/
internal fun Project.createPublishTasks() {
    tasks.register<Delete>(TASK_CLEAN_SUBMISSION) {
        delete(runeMateBuildDir)
    }
    tasks.register<GenerateManifests>(TASK_GENERATE_MANIFESTS) {
        dependsOn(TASK_CLEAN_SUBMISSION)
    }
    tasks.register<ValidateManifests>(TASK_VALIDATE_MANIFESTS) {
        dependsOn(TASK_GENERATE_MANIFESTS)
    }

    tasks.findByName("jar")?.apply {
        dependsOn(TASK_GENERATE_MANIFESTS)
        (this as Jar).from(runeMateManifestDir)
    }

    tasks.register<Copy>(TASK_COLLECT_SOURCES) {
        dependsOn(TASK_VALIDATE_MANIFESTS)
        sourceRoots.forEach {
            from(it)
            into(runeMateSourcesDir)
        }

        // Don't collect source code from modules where:
        // 1. all manifests declared are to be skipped
        // 2. the module has explicitly had submission disabled
        enabled = !excludeFromSubmission
    }

    //These two tasks should only be registered to the root project
    if (this == rootProject) {
        tasks.register(TASK_BUILD_SUBMISSION, Tar::class) {
            //Always run a clean and recollect sources to ensure bundle is up-to-date
            val projects = allprojects.filterNot { it.excludeFromSubmission }
            projects.forEach { dependsOn(it.tasks[TASK_COLLECT_SOURCES]) }
            projects.forEach { from(it.runeMateSourcesDir) }

            compression = Compression.GZIP
            destinationDirectory.set(runeMateDistributionDir)
            archiveFileName.set("runemate-publish.tar.gz")
        }

        tasks.register<Submit>("submitForReview") {
            dependsOn(TASK_BUILD_SUBMISSION)
        }
    }
}

open class GenerateManifests : DefaultTask() {

    @TaskAction
    fun run() {
        val ext = project.extensions.getByType<RuneMateExtension>()
        val manifests = ext.manifests
            .filterNot { it.skip }
            .map { validate("generate ${it.name}", it.toManifest()) }
            .associateBy { it.name.toManifestName() }
            .mapValues { it.value.toJson() }

        if (manifests.isNotEmpty()) {
            val directory = project.runeMateManifestDir.get()
            manifests.forEach { (key, manifest) ->
                try {
                    val file = directory.file("$key.manifest.json").asFile.toPath()
                    logger.debug("Writing manifest {}", file.relativeTo(project.rootDir.toPath()))
                    Files.createDirectories(directory.asFile.toPath())
                    file.writeText(manifest)
                } catch (e: Exception) {
                    logger.warn("Failed to write manifest '$key'", e)
                }
            }

            logger.debug("Generated {} manifests: {}", manifests.size, manifests.keys.map { "$it.manifest.json" })
        }
    }

    private fun String.toManifestName() = lowercase().replace(Regex("[^\\\\dA-Za-z0-9 ]"), "").replace(" ", "-")

}

open class ValidateManifests : DefaultTask() {

    init {
        group = "runemate publish"
        description = "Scans project source for manifests and validates them"
    }

    @TaskAction
    fun run() {
        //Validate manifests located in the project source (not including generated manifests)
        val manifests = project.sourceRoots
            .flatMap { project.fileTree(it).files }
            .filter { isManifest(it) }
            .associate { f -> "file ${f.toPath().relativeTo(project.rootDir.toPath())}" to project.parse(f) }
            .filterValues { it != null }
            .onEach { (key, manifest) -> validate(key, manifest!!) }

        logger.debug("Validated contents of ${manifests.size} manifests")

        //Check for duplicate internalIds
        for (manifest in manifests.values.distinctBy { it?.internalId }) {
            val count = manifests.count { it.value?.internalId == manifest?.internalId }
            if (count > 1) {
                val duplicates = manifests.filterValues { it?.internalId == manifest?.internalId }.keys
                throw GradleException("$count manifests have the same internalId '${manifest?.internalId}': $duplicates")
            }
        }
    }

}

open class Submit : DefaultTask() {

    init {
        group = "runemate publish"
        description = "Submits all source code to the store"
    }

    @TaskAction
    fun run() {
        val ext = project.extensions.getByType<RuneMateExtension>()
        val file = project.tasks.getByName(TASK_BUILD_SUBMISSION).outputs.files.singleFile

        logger.lifecycle("Submitting project '${project.name}' for review")

        val submissionToken = ext.submissionToken.orNull
                ?: project.findProperty("runemateSubmissionToken")?.toString()
                ?: System.getProperty("runemateSubmissionToken")
                ?: System.getenv("RUNEMATE_SUBMISSION_TOKEN")
                ?: throw GradleException("Unable to locate submission token from extension property, project property, system property or environment variable")

        val okhttp = OkHttpClient()
        val form = MultipartBody.Builder()
            .setType(MultipartBody.FORM)
            .addFormDataPart("file", "bundle.tar.gz", file.asRequestBody("application/gzip".toMediaType()))
            .build()

        val request = Request.Builder()
            .header("Authorization", "Bearer $submissionToken")
            .post(form)
            .url(SUBMISSION_URL)
            .build()

        okhttp.newCall(request).execute().use { response ->
            if (response.isSuccessful) {
                try {
                    val body = jsonMapper.readValue<SubmitResponse>(response.body!!.charStream())
                    logger.lifecycle("Submission successful - you will receive a forum message when your submission has been reviewed")
                    logger.lifecycle("Submission ID: ${body.id}")
                } catch (e: IOException) {
                    logger.warn("Submission was successful (but we failed to parse the response from the server)")
                }
                return
            }

            when (response.code) {
                /*Unauthorized*/
                401 -> throw GradleException("Submission failed: Missing submission key")
                /*Forbidden*/
                403 -> throw GradleException("Submission rejected: Invalid submission key")
                in 403..499 -> {
                    try {
                        val content = response.body?.use { it.string() }
                        var error: String? = null
                        if (content != null) {
                            val errorResponse = jsonMapper.readValue<ErrorResponse>(content)
                            error = errorResponse.errors.firstOrNull()?.message
                        }
                        if (error == null) {
                            logger.debug("Unable to locate error from response body: {}", content)
                            error = "Unknown error"
                        }
                        throw GradleException("Submission rejected: $error")
                    } catch (e: IOException) {
                        throw GradleException("Unable to parse response from rejected submission (code ${response.code})", e)
                    }
                }

                else -> {
                    throw GradleException("Submission failed for an unknown reason (code ${response.code}) - please contact the RuneMate team")
                }
            }
        }
    }
}

data class ErrorResponse(
    val errorId: String,
    val statusCode: Int,
    val errors: List<ErrorMessage> = emptyList(),
)

data class ErrorMessage(
    val message: String,
    val path: String? = null,
)

private data class SubmitResponse(
    val id: UUID,
)