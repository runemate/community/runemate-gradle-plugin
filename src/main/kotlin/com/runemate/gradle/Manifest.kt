import com.fasterxml.jackson.databind.MapperFeature
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.databind.exc.InvalidFormatException
import com.fasterxml.jackson.databind.json.JsonMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import com.runemate.metadata.bot.BotManifest
import com.runemate.metadata.bot.ManifestRule
import com.runemate.metadata.bot.parseManifest
import org.gradle.api.GradleException
import org.gradle.api.Project
import java.io.File

internal val jsonMapper = JsonMapper.builder()
    .enable(MapperFeature.ACCEPT_CASE_INSENSITIVE_ENUMS)
    .disable(SerializationFeature.WRITE_DURATIONS_AS_TIMESTAMPS)
    .build()
    .registerKotlinModule()
    .registerModule(JavaTimeModule())

internal fun Project.parse(file: File): BotManifest? {
    try {
        return parseManifest(file)
    } catch (e: InvalidFormatException) {
        logger.error("Invalid value in {}: {}", file, e.message)
    } catch (e: Exception) {
        logger.lifecycle("{} was not parseable as a manifest", file, e)
    }
    return null
}

internal fun validate(source: String, manifest: BotManifest) : BotManifest {
    ManifestRule.entries.forEach { rule ->
        if (rule.rejects(manifest)) {
            throw GradleException("Invalid manifest: ${rule.rejection} ($source)")
        }
    }
    return manifest
}

internal fun BotManifest.toJson(): String = jsonMapper.writerWithDefaultPrettyPrinter().writeValueAsString(this)