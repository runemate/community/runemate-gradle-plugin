package com.runemate.gradle

import com.runemate.metadata.bot.BotManifest
import com.runemate.metadata.bot.Trial
import com.runemate.metadata.bot.Variant
import com.runemate.game.api.bot.data.*
import org.gradle.api.Action
import org.gradle.api.GradleException
import org.gradle.api.Named
import org.gradle.kotlin.dsl.provideDelegate
import java.math.BigDecimal
import java.time.Duration
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

class ManifestDeclaration(@JvmField val name: String) : Named {

    var skip: Boolean = false
    var mainClass: String by required()
    var tagline: String by required()
    var description: String by required()
    var version: String by required()
    var internalId: String by required()
    var compatibility: Set<GameType> = mutableSetOf(GameType.OSRS)
    var categories: List<Category> = mutableListOf(Category.OTHER)
    var features: Set<Feature> = mutableSetOf()
    var access: Access = Access.PUBLIC
    var hidden: Boolean = false

    var trial: Trial? = null
    var resources: Set<String> = emptySet()
    var tags: Set<String> = emptySet()
    var obfuscation: Set<String> = emptySet()
    var variants: List<Variant> = listOf()

    @Deprecated(replaceWith = ReplaceWith("variants { variant(name, price) }"), message = "Replaced by variants")
    fun price(price: Double) {
        variants {
            variant(name, price)
        }
    }

    fun tags(vararg tags: String) {
        this.tags = tags.toSet()
    }

    fun categories(vararg category: Category) {
        this.categories = category.toList()
    }

    fun obfuscation(block: Action<ObfuscationSpec>) {
        val spec = ObfuscationSpec()
        block.execute(spec)
        this.obfuscation = spec.exclusions
    }

    fun resources(block: Action<ResourcesSpec>) {
        val spec = ResourcesSpec()
        block.execute(spec)
        this.resources = spec.resources
    }

    fun trial(block: Action<TrialSpec>) {
        val spec = TrialSpec()
        block.execute(spec)
        trial = Trial(allowance = spec.allowance, window = spec.window)
    }

    fun features(block: Action<FeatureSpec>) {
        val spec = FeatureSpec()
        block.execute(spec)
        features = spec.features
    }

    fun variants(block: Action<VariantsSpec>) {
        val spec = VariantsSpec()
        block.execute(spec)
        variants = spec.variants.map { Variant(null, it.name, it.price) }.toList()
    }

    internal fun toManifest() = BotManifest(
            mainClass,
            name,
            tagline,
            description,
            version,
            internalId,
            compatibility,
            categories,
            features,
            access,
            hidden,
            false,
            trial,
            resources,
            tags,
            obfuscation,
            variants.toMutableList()
    )

    override fun getName(): String = name
}

class ObfuscationSpec {

    internal val exclusions: MutableSet<String> = mutableSetOf()

    fun exclude(block: () -> String) {
        exclude(block())
    }

    fun exclude(exclusion: String) {
        exclusions += exclusion
    }

    operator fun String.unaryPlus() {
        exclude(this)
    }

}

class ResourcesSpec {

    internal val resources: MutableSet<String> = mutableSetOf()

    fun include(block: () -> String) {
        include(block())
    }

    fun include(rule: String) {
        resources += rule
    }

    operator fun String.unaryPlus() {
        include(this)
    }

}

class TrialSpec {

    var allowance: Duration = Duration.ZERO
    var window: Duration = Duration.ZERO

    fun allowance(duration: String) {
        allowance = Duration.parse(duration)
    }

    fun window(duration: String) {
        window = Duration.parse(duration)
    }
}

class FeatureSpec {

    internal val features: MutableSet<Feature> = mutableSetOf()

    fun required(feature: FeatureType) {
        features += Feature(feature, FeatureMode.REQUIRED)
    }

    fun optional(feature: FeatureType) {
        features += Feature(feature, FeatureMode.OPTIONAL)
    }

    fun required(feature: () -> FeatureType) {
        features += Feature(feature(), FeatureMode.REQUIRED)
    }

    fun optional(feature: () -> FeatureType) {
        features += Feature(feature(), FeatureMode.OPTIONAL)
    }
}

private inline fun <reified T> ManifestDeclaration.required() = RequiredManifestElement<T>(this)

class VariantsSpec {

    internal val variants: MutableSet<VariantSpec> = mutableSetOf()

    fun variant(block: Action<VariantSpec>) {
        val spec = VariantSpec()
        block.execute(spec)
        variants += spec
    }

    fun variant(name: String, price: BigDecimal) = variant {
        name(name)
        price(price)
    }

    fun variant(name: String, price: Double) = variant {
        name(name)
        price(price)
    }
}

class VariantSpec {

    var name: String = ""
    var price: BigDecimal = BigDecimal.ZERO

    fun name(value: String) {
        name = value
    }

    fun name(block: () -> String) = name(block())

    fun price(price: Double) {
        this.price = price.toBigDecimal()
    }

    fun price(price: BigDecimal) {
        this.price = price
    }
}

private class RequiredManifestElement<T>(private val dec: ManifestDeclaration) : ReadWriteProperty<Any?, T> {

    private var value: T? = null

    override fun getValue(thisRef: Any?, property: KProperty<*>): T {
        return value ?: throw GradleException("Missing property '${property.name}' in ${dec.name}")
    }

    override fun setValue(thisRef: Any?, property: KProperty<*>, value: T) {
        this.value = value
    }

    override fun toString(): String = "ManifestProperty(${if (value != null) "value=$value" else "value not initialized yet"})"
}