import org.apache.tools.ant.filters.ReplaceTokens
import org.jetbrains.kotlin.gradle.dsl.KotlinVersion

plugins {
    kotlin("jvm") version "1.9.0"
    `kotlin-dsl`
    `java-gradle-plugin`
    id("com.gradle.plugin-publish") version "1.2.1"
    id("com.github.johnrengelman.shadow") version "7.1.2"
}

group = "com.runemate"
version = "1.5.1"

gradlePlugin {
    website = "https://www.runemate.com"
    vcsUrl = "https://gitlab.com/runemate/community/runemate-gradle-plugin.git"
    plugins {
        create("runemateGradlePlugin") {
            id = "com.runemate"
            implementationClass = "com.runemate.gradle.RuneMatePlugin"
            description = "Configures your Gradle project for use with RuneMate"
            displayName = "RuneMate Development Plugin"
            tags = setOf("runemate", "development")
        }
    }
}

repositories {
    mavenCentral()
    gradlePluginPortal()
    maven("https://gitlab.com/api/v4/projects/10471880/packages/maven")
    mavenLocal()
}

dependencies {
    shadow(gradleApi())
    shadow(kotlin("gradle-plugin"))
    shadow("org.gradle.kotlin:gradle-kotlin-dsl-plugins:4.4.0")
    shadow("com.squareup.okhttp3:okhttp:4.12.0")
    shadow("org.openjfx:javafx-plugin:0.1.0")
    shadow(platform("com.fasterxml.jackson:jackson-bom:2.14.0"))
    shadow("com.fasterxml.jackson.core:jackson-core")
    shadow("com.fasterxml.jackson.core:jackson-databind")
    shadow("com.fasterxml.jackson.core:jackson-annotations")
    shadow("com.fasterxml.jackson.module:jackson-module-kotlin")
    shadow("com.fasterxml.jackson.datatype:jackson-datatype-jsr310")
    shadow("com.fasterxml.jackson.datatype:jackson-datatype-jdk8")
    shadow("com.fasterxml.jackson.dataformat:jackson-dataformat-yaml")

    implementation("com.runemate:runemate-bot-metadata:1.0.1")
}

kotlin {
    jvmToolchain {
        languageVersion.set(JavaLanguageVersion.of(17))
    }
}

tasks.compileKotlin {
    compilerOptions {
        languageVersion = KotlinVersion.KOTLIN_1_9
        apiVersion = KotlinVersion.KOTLIN_1_9
    }
}

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(17))
    }
}

tasks.shadowJar {
    archiveClassifier.set("")
}

publishing {
    repositories {
        mavenLocal()
    }
}

val generateReadMe by tasks.registering(Copy::class) {
    doNotTrackState("")
    from("$rootDir/.templates/readme.md") {
        val tokens = mapOf("version" to project.version)
        filter(ReplaceTokens::class, mapOf("tokens" to tokens))
    }
    into(rootDir)
}

tasks.build {
    finalizedBy(generateReadMe)
}
